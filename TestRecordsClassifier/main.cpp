#include <vld.h>
#include "testrecordslexer.h"
#include "testrecordsparser.h"
#include "testrulesparser.h"
#include "testanalyzer.h"

int main(int argc, char *argv[])
{
    QTest::qExec(&TestRecordsLexer(), argc, argv);
    QTest::qExec(&TestRecordsParser(), argc, argv);
    QTest::qExec(&TestRulesParser(), argc, argv);
    QTest::qExec(&TestAnalyzer(), argc, argv);

    return 0;
}
