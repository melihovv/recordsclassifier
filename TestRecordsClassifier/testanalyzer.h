#ifndef TESTANALYZER_H
#define TESTANALYZER_H

#include <QObject>
#include <QTest>
#include "../RecordsClassifier/analyzer.h"
#include "declarations.h"

typedef QList<SetOfRules> SetsOfRules;
typedef QList<Rule> Rules;
typedef QList<Record> Records;
typedef QList<Property> Properties;
typedef QList<int> Numbers;
typedef QMap<QString, QStringList> Expectation;

class TestAnalyzer : public QObject
{
    Q_OBJECT

private slots:
    void testAnalyze_data();
    void testAnalyze();
};

#endif // TESTANALYZER_H
