#ifndef TESTRECORDSLEXER_H
#define TESTRECORDSLEXER_H

#include <QObject>
#include <QTest>
#include "../RecordsClassifier/recordslexer.hpp"
#include "../RecordsClassifier/recordsparser.hpp"

class TestRecordsLexer : public QObject
{
    Q_OBJECT

private slots:
    void cleanup();

    void testYylex_data();
    void testYylex();
};

#endif // TESTRECORDSLEXER_H
