#include "testrecordslexer.h"

void TestRecordsLexer::cleanup()
{
    xxlex_destroy();
}

void TestRecordsLexer::testYylex_data()
{
    QTest::addColumn<QString>("buffer");
    QTest::addColumn<int>("expectation");
    QTest::addColumn<QString>("valueType");

    QTest::newRow("=")
        << "="
        << (int) '='
        << "";

    QTest::newRow(",")
        << ","
        << (int) ','
        << "";

    QTest::newRow("{")
        << "{"
        << (int) '{'
        << "";

    QTest::newRow("}")
        << "}"
        << (int) '}'
        << "";

    QTest::newRow("@")
        << "@"
        << (int) '@'
        << "";

    QTest::newRow("\n")
        << "\n"
        << 0
        << "";

    QTest::newRow("\r\n")
        << "\r\n"
        << 0
        << "";

    QTest::newRow("\r")
        << "\r"
        << 0
        << "";

    QTest::newRow("Space")
        << " "
        << 0
        << "";

    QTest::newRow("Tab")
        << "\t"
        << 0
        << "";

    QTest::newRow("Number")
        << "12"
        << (int) XXNUMBER
        << "intVal";

    QTest::newRow("Id")
        << "someId12"
        << (int) XXID
        << "strVal";
}

void TestRecordsLexer::testYylex()
{
    QFETCH(QString, buffer);
    QFETCH(int, expectation);
    QFETCH(QString, valueType);

    xx_scan_string(buffer.toStdString().c_str());
    int real = xxlex();
    QVERIFY2(real == expectation, QString("real: %1, expectation: %2").arg(real).arg(expectation).toStdString().c_str());

    if (valueType == "strVal")
    {
        QVERIFY2(buffer == *xxlval.strVal, QString("buffer: %1, xxlval: %2").arg(buffer).arg(*xxlval.strVal).toStdString().c_str());

        delete xxlval.strVal;
        xxlval.strVal = nullptr;
    }
    else if (valueType == "intVal")
    {
        QVERIFY2(buffer.toInt() == xxlval.intVal, QString("buffer: %1, xxlval: %2").arg(buffer).arg(xxlval.intVal).toStdString().c_str());
    }
}
