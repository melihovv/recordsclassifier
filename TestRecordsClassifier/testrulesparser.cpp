#include "testrulesparser.h"

void TestRulesParser::parse(const QString& text)
{
    zzcolumn = 1;
    zz_scan_string(text.toStdString().c_str());
    zzparse();
    zzlex_destroy();
}

void TestRulesParser::cleanup()
{
    setsOfRules.clear();
}

void TestRulesParser::testYyparse_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<SetsOfRules>("expected");

    QTest::newRow("Empty array of numbers")
        << "@name\nrule = 1 2 {}"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule", 1, 2, Numbers())}))});

    QTest::newRow("One set of rules, one rule")
        << "@name\nrule = 1 2 {1, 2, 3}"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule", 1, 2, Numbers({1, 2, 3}))}))});

    QTest::newRow("One set of rules, two rules")
        << "@name\nrule1 = 1 2 {1, 2, 3}\nrule2 = 1 2 {1, 2, 3}"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))}))});

    QTest::newRow("Several sets of rules")
        << "@name1\nrule1 = 1 2 {1, 2, 3}\nrule2 = 1 2 {1, 2, 3}\n@name2\nrule1 = 1 2 {1, 2, 3}\nrule2 = 1 2 {1, 2, 3}"
        << SetsOfRules({SetOfRules("name1", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))})), SetOfRules("name2", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))}))});
}

void TestRulesParser::testYyparse()
{
    QFETCH(QString, text);
    QFETCH(SetsOfRules, expected);

    parse(text);
    QVERIFY(setsOfRules == expected);
}
