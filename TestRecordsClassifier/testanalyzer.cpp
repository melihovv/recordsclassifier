#include "testanalyzer.h"

void TestAnalyzer::testAnalyze_data()
{
    QTest::addColumn<Records>("records");
    QTest::addColumn<SetsOfRules>("setsOfRules");
    QTest::addColumn<Expectation>("expectation");

    QTest::newRow("No matches")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("rule", 1, 0, Numbers())}))})
        << Expectation();

    QTest::newRow("Rule 1")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 1, 0, Numbers())}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 1, two classes match one record")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class1", Rules({Rule("property", 1, 0, Numbers())})), SetOfRules("class2", Rules({Rule("property", 1, 0, Numbers())}))})
        << Expectation{{"record", QStringList({"class1", "class2"})}};

    QTest::newRow("Rule 1, two classes, two records, each class matches one record")
        << Records({Record("record1", Properties({Property("property1", Numbers({1, 2, 3}))})), Record("record2", Properties({Property("property2", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class1", Rules({Rule("property1", 1, 0, Numbers())})), SetOfRules("class2", Rules({Rule("property2", 1, 0, Numbers())}))})
        << Expectation{{"record1", QStringList({"class1"})}, {"record2", QStringList({"class2"})}};

    QTest::newRow("Rule 2")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 2, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 3")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 3, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 4")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 4, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};
}

void TestAnalyzer::testAnalyze()
{
    QFETCH(Records, records);
    QFETCH(SetsOfRules, setsOfRules);
    QFETCH(Expectation, expectation);

    Expectation result = Analyzer(records, setsOfRules).analyze();
    QVERIFY(result == expectation);
}
