#ifndef TESTRECORDSPARSER_H
#define TESTRECORDSPARSER_H

#include <QObject>
#include <QTest>
#include <QList>
#include "../RecordsClassifier/recordslexer.hpp"
#include "../RecordsClassifier/recordsparser.hpp"
#include "../RecordsClassifier/record.h"
#include "declarations.h"

extern int xxcolumn;
extern QList<Record> records;

class TestRecordsParser : public QObject
{
    Q_OBJECT

private:
    void parse(const QString &text);

private slots:
    void cleanup();

    void testYyparse_data();
    void testYyparse();
};

#endif // TESTRECORDSPARSER_H
