#include "testrecordsparser.h"

void TestRecordsParser::parse(const QString& text)
{
    xxcolumn = 1;
    xx_scan_string(text.toStdString().c_str());
    xxparse();
    xxlex_destroy();
}

void TestRecordsParser::cleanup()
{
    records.clear();
}

void TestRecordsParser::testYyparse_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<QList<Record>>("expected");

    typedef QList<Record> Records;
    typedef QList<Property> Properties;
    typedef QList<int> Numbers;

    QTest::newRow("Empty array of numbers")
        << "@name\nproperty = {}"
        << Records({Record("name", Properties({Property("property", Numbers())}))});

    QTest::newRow("One record, one property")
        << "@name\nproperty = {1, 2, 3}"
        << Records({Record("name", Properties({Property("property", Numbers({1, 2, 3}))}))});

    QTest::newRow("One record, two properties")
        << "@name\nproperty1 = {1, 2, 3}\nproperty2 = {4, 5, 6}"
        << Records({Record("name", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});

    QTest::newRow("Several records")
        << "@name1\nproperty1 = {1, 2, 3}\nproperty2 = {4, 5, 6}\n\n@name2\nproperty1 = {1, 2, 3}\nproperty2 = {4, 5, 6}"
        << Records({Record("name1", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))})), Record("name2", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});
}

void TestRecordsParser::testYyparse()
{
    QFETCH(QString, text);
    QFETCH(QList<Record>, expected);

    parse(text);
    QVERIFY(records == expected);
}
