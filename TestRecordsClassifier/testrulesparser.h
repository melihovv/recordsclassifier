#ifndef TESTRULESPARSER_H
#define TESTRULESPARSER_H

#include <QObject>
#include <QTest>
#include <QList>
#include "../RecordsClassifier/ruleslexer.hpp"
#include "../RecordsClassifier/rulesparser.hpp"
#include "../RecordsClassifier/rule.h"
#include "declarations.h"

extern int zzcolumn;
extern QList<SetOfRules> setsOfRules;

typedef QList<SetOfRules> SetsOfRules;
typedef QList<Rule> Rules;
typedef QList<int> Numbers;

class TestRulesParser : public QObject
{
    Q_OBJECT

private:
    void parse(const QString &text);

private slots:
    void cleanup();

    void testYyparse_data();
    void testYyparse();
};

#endif // TESTRULESPARSER_H
