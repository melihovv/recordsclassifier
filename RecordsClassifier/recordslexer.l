%option noyywrap never-interactive outfile="recordslexer.cpp" header-file="recordslexer.hpp" yylineno prefix="xx"

%top {
    #pragma warning(disable: 4996)
}

%{
    #include "unistd.h"
    #include "recordsparser.hpp"

    int xxcolumn = 1;
    char *xxfilename;

    #define XX_USER_ACTION \
        xxlloc.filename = filename; \
        xxlloc.first_line = xxlloc.last_line = xxlineno; \
        xxlloc.first_column = xxcolumn; \
        xxlloc.last_column = xxcolumn + xxleng - 1; \
        xxcolumn += xxleng;

    void xxerror(const char *msg)
    {
        printf("%s:%d.%d: %s\n", xxlloc.filename, xxlloc.first_line, xxlloc.first_column, msg);
    }
%}

digit [0-9]
number {digit}+

letter [a-zA-Z]
id {letter}({letter}|{digit})*

cr [\r]
lf [\n]
eol ({cr}{lf}|{lf}|{cr})

space [ ]
tab [\t]
spaces ({space}|{tab})

%%
"," {return xxtext[0];}
"=" {return xxtext[0];}
"{" {return xxtext[0];}
"}" {return xxtext[0];}
"@" {return xxtext[0];}

{spaces} {}
{eol} {xxcolumn = 1;}
{number} {xxlval.intVal = atoi(xxtext); return XXNUMBER;}
{id} {xxlval.strVal = new QString(xxtext); return XXID;}
%%
