/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Worker class. Reads files, write file.
 */

#ifndef WORKER_H
#define WORKER_H

#include <QString>
#include <QFile>
#include <QTextStream>
#include "recordslexer.hpp"
#include "recordsparser.hpp"
#include "ruleslexer.hpp"
#include "rulesparser.hpp"
#include "analyzer.h"

typedef QList<Record> Records;
typedef QList<SetOfRules> SetsOfRules;
typedef QMap<QString, QStringList> RecordsToClassesMap;

extern char *xxfilename;
extern char *zzfilename;
extern int xxnerrs;
extern int zznerrs;
extern Records records;
extern SetsOfRules setsOfRules;

/*!
 * Worker class.
 */
class Worker
{
public:
    Worker(const QString &recordsFilePath, const QString &rulesFilePath) throw (const QString &);
    ~Worker();

    /* Reads files, call Analyzer.analyze() and writes result to file. */
    void run() throw (const QString &);

private:
    void writeResultToFile(const RecordsToClassesMap &recordsToClassesMap) throw (const QString &);

    QString recordsFileContent;
    QString recordsFilePath;
    QString rulesFileContent;
    QString rulesFilePath;
};

#endif // WORKER_H
