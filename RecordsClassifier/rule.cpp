#include "rule.h"

Rule::Rule(const QString &name, int id, int number, const QList<int> &numbers)
{
    this->name = name;
    this->id = id;
    this->number = number;
    this->numbers = numbers;
}

Rule::~Rule()
{
}

bool Rule::operator==(const Rule &rule)
{
    return
        this->name == rule.name &&
        this->id == rule.id &&
        this->number == rule.number &&
        this->numbers == rule.numbers;
}
