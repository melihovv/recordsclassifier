/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Record class.
 */

#ifndef RECORD_H
#define RECORD_H

#include <QString>
#include <QList>
#include "property.h"

/*!
 * Record class.
 */
class Record
{
public:
    /*! Construct an instance of class. */
    Record(const QString &name, const QList<Property> &properties);
    ~Record();

    /*! Compare operator. */
    bool operator==(const Record &record);

    QString getName() const
    {
        return name;
    }

    QList<Property> getProperties() const
    {
        return properties;
    }

private:
    QString name;
    QList<Property> properties;
};

#endif // RECORD_H
