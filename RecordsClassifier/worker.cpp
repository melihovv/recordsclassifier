#include "worker.h"

Worker::Worker(const QString &recordsFilePath, const QString &rulesFilePath)
{
    QFile recordsFile(recordsFilePath);
    if (!recordsFile.open(QIODevice::ReadOnly))
    {
        throw QString("Couldn`t open file ") + recordsFilePath;
    }

    QFile rulesFile(rulesFilePath);
    if (!rulesFile.open(QIODevice::ReadOnly))
    {
        throw QString("Couldn`t open file ") + rulesFilePath;
    }

    QTextStream input1(&recordsFile);
    recordsFileContent = input1.readAll();
    QTextStream input2(&rulesFile);
    rulesFileContent = input2.readAll();

    xxfilename = recordsFilePath.toLocal8Bit().data();
    zzfilename = rulesFilePath.toLocal8Bit().data();

    this->recordsFilePath = recordsFilePath;
    this->rulesFilePath = rulesFilePath;
}

Worker::~Worker()
{
}

void Worker::run()
{
    xx_scan_string(recordsFileContent.toStdString().c_str());
    int parseResult = xxparse();
    xxlex_destroy();

    switch (parseResult)
    {
        case 0: break;
        case 1: throw QString("There are syntax errors"); break;
        case 2: throw QString("Bison: error while freeing memory"); break;
    }

    if (xxnerrs)
    {
        throw QString("Couldn`t parse file " + recordsFilePath);
    }

    zz_scan_string(rulesFileContent.toStdString().c_str());
    parseResult = zzparse();
    zzlex_destroy();

    switch (parseResult)
    {
        case 0: break;
        case 1: throw QString("There are syntax errors"); break;
        case 2: throw QString("Bison: error while freeing memory"); break;
    }

    if (zznerrs)
    {
        throw QString("Couldn`t parse file " + rulesFilePath);
    }

    Analyzer analyzer(records, setsOfRules);
    writeResultToFile(analyzer.analyze());
}

void Worker::writeResultToFile(const RecordsToClassesMap &recordsToClassesMap)
{
    QFile file("result.txt");
    if (!file.open(QIODevice::WriteOnly))
    {
        throw QString("Cannot create file for result output");
    }

    QTextStream out(&file);
    foreach(QString key, recordsToClassesMap.keys())
    {
        auto setsOfRules = recordsToClassesMap.value(key);
        out << key << " = " << setsOfRules.join(", ") << endl;
    }
}
