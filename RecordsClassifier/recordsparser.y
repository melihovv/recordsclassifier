%defines
%locations
%error-verbose
%define api.prefix {xx}

%code requires
{
    typedef struct XXLTYPE
    {
        int first_line;
        int first_column;
        int last_line;
        int last_column;
        char *filename;
    } XXLTYPE;

    #define XXLTYPE_IS_DECLARED 1

    #define XXLLOC_DEFAULT(Current, Rhs, N) \
        do \
            if (N) \
            { \
                (Current).first_line   = XXRHSLOC (Rhs, 1).first_line;   \
                (Current).first_column = XXRHSLOC (Rhs, 1).first_column; \
                (Current).last_line    = XXRHSLOC (Rhs, N).last_line;    \
                (Current).last_column  = XXRHSLOC (Rhs, N).last_column;  \
                (Current).filename     = XXRHSLOC (Rhs, 1).filename;     \
            } \
            else \
           {                                                             \
                (Current).first_line   = (Current).last_line   =         \
                XXRHSLOC (Rhs, 0).last_line;                             \
                (Current).first_column = (Current).last_column =         \
                XXRHSLOC (Rhs, 0).last_column;                           \
                (Current).filename  = NULL;                              \
            }                                                            \
        while (0)

    #include <QString>
    #include "record.h"
}

%{
    #pragma warning(disable: 4996)
    #include <QString>
    #include <QList>
    #include "record.h"

    QList<Record> records;

    extern int xxlex();
    extern void xxerror(const char *msg);
%}

%union
{
    int intVal;
    QString *strVal;
    Record *record;
    Property *property;
    QList<Property> *properties;
    QList<int> *numbers;
}

%token<intVal> XXNUMBER
%token<strVal> XXID

%type<property> Property
%type<properties> Properties
%type<record> Record
%type<numbers> Numbers

%start Records

%%
Records
    : Record {records << *$1; delete $1;}
    | Records Record {records << *$2; delete $2;}
    ;

Record
    : '@' XXID Properties {$$ = new Record(*$2, *$3); delete $2; delete $3;}
    ;

Properties
    : Property {$$ = new QList<Property>(); *$$ << *$1; delete $1;}
    | Properties Property {*$$ << *$2; delete $2;}
    ;

Property
    : XXID '=' '{' Numbers '}' {$$ = new Property(*$1, *$4); delete $1; delete $4;}
    | XXID '=' '{' '}' {$$ = new Property(*$1, QList<int>()); delete $1;}
    ;

Numbers
    : XXNUMBER {$$ = new QList<int>(); *$$ << $1;}
    | Numbers ',' XXNUMBER {*$$ << $3;}
    ;
%%
