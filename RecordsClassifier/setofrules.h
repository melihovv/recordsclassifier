/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Set of rules class.
 */

#ifndef SETOFRULES_H
#define SETOFRULES_H

#include "rule.h"

/*!
 * Set of rules class.
 */
class SetOfRules
{
public:
    /*! Construct an instance of class. */
    SetOfRules(const QString &name, const QList<Rule> &rules);
    ~SetOfRules();

    /*! Compare operator. */
    bool operator==(const SetOfRules &setOfRules);

    QString getName() const
    {
        return name;
    }

    QList<Rule> getRules() const
    {
        return rules;
    }

private:
    QString name;
    QList<Rule> rules;
};

#endif // SETOFRULES_H
