#include "property.h"

Property::Property(const QString &name, const QList<int> &values)
{
    this->name = name;
    this->numbers = values;
}

Property::~Property()
{
}

bool Property::operator==(const Property& property)
{
    return
        this->name == property.name &&
        this->numbers == property.numbers;
}
