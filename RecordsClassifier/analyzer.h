/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Analyzer class. Analazies which record belongs to which class.
 */

#ifndef ANALYZER_H
#define ANALYZER_H

#include <QMap>
#include <QStringList>
#include "record.h"
#include "setofrules.h"

typedef QMap<QString, QStringList> RecordsToClassesMap;

/*!
 * Analyzer class.
 */
class Analyzer
{
public:
    /*! Construct an instance of class. */
    Analyzer(const QList<Record> &records, const QList<SetOfRules> &setsOfRules);
    ~Analyzer();

    /*! Analyzes which record belongs to which class. */
    RecordsToClassesMap analyze();

private:
    /*! Adds value as QStringList if it is not exists, otherwise append to existing one. */
    void addValueToMap(RecordsToClassesMap &map, const QString &key, const QString &value);

    QList<Record> records;
    QList<SetOfRules> setsOfRules;
};

#endif // ANALYZER_H
