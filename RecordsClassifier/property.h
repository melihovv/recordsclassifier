/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Property class.
 */

#ifndef PROPERTY_H
#define PROPERTY_H

#include <QString>
#include <QList>

/*!
 * Property class.
 */
class Property
{
public:
    /*! Construct an instance of class. */
    Property(const QString &name, const QList<int> &values);
    ~Property();

    /*! Compare operator. */
    bool operator==(const Property &property);

    QString getName() const
    {
        return name;
    }

    QList<int> getNumbers() const
    {
        return numbers;
    }

private:
    QString name;
    QList<int> numbers;
};

#endif // PROPERTY_H
