/*!
 *\file
 *\author Alexander Melihov <amelihovv@ya.ru>
 * Rule class.
 */

#ifndef RULE_H
#define RULE_H

#include <QString>
#include <QList>

/*!
 * Rule class.
 */
class Rule
{
public:
    /*! Construct an instance of class. */
    Rule(const QString &name, int id, int number, const QList<int> &numbers);
    ~Rule();

    /*! Compare operator. */
    bool operator==(const Rule &rule);

    QString getName() const
    {
        return name;
    }

    int getId() const
    {
        return id;
    }

    int getNumber() const
    {
        return number;
    }

    QList<int> getNumbers() const
    {
        return numbers;
    }

private:
    QString name;
    int id;
    int number;
    QList<int> numbers;
};

#endif // RULE_H

