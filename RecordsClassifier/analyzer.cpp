#include <QString>
#include "analyzer.h"

Analyzer::Analyzer(const QList<Record> &records, const QList<SetOfRules> &setsOfRules)
{
    this->records = records;
    this->setsOfRules = setsOfRules;
}

Analyzer::~Analyzer()
{
}

RecordsToClassesMap Analyzer::analyze()
{
    QMap<QString, QStringList> result;

    auto addValueToMap = [&result](QString key, QString value)
    {
        if (result.contains(key))
        {
            result[key].append(value);
        }
        else
        {
            result.insert(key, QStringList(value));
        }
    };

    for (auto setOfRules : setsOfRules)
    {
        for (auto rule : setOfRules.getRules())
        {
            for (auto record : records)
            {
                for (auto property : record.getProperties())
                {
                    if (property.getName() == rule.getName())
                    {
                        switch (rule.getId())
                        {
                        case 1:
                            addValueToMap(record.getName(), setOfRules.getName());
                            break;
                        case 2:
                            if (rule.getNumber() == property.getNumbers().length())
                            {
                                addValueToMap(record.getName(), setOfRules.getName());
                            }
                            break;
                        case 3:
                            if (property.getNumbers().contains(rule.getNumber()))
                            {
                                addValueToMap(record.getName(), setOfRules.getName());
                            }
                            break;
                        case 4:
                            if (rule.getNumbers() == property.getNumbers())
                            {
                                addValueToMap(record.getName(), setOfRules.getName());
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    return result;
}
