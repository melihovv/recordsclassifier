%defines
%locations
%error-verbose
%define api.prefix {zz}

%code requires
{
    typedef struct ZZLTYPE
    {
        int first_line;
        int first_column;
        int last_line;
        int last_column;
        char *filename;
    } ZZLTYPE;

    #define ZZLTYPE_IS_DECLARED 1

    #define ZZLLOC_DEFAULT(Current, Rhs, N) \
        do \
            if (N) \
            { \
                (Current).first_line   = ZZRHSLOC (Rhs, 1).first_line;   \
                (Current).first_column = ZZRHSLOC (Rhs, 1).first_column; \
                (Current).last_line    = ZZRHSLOC (Rhs, N).last_line;    \
                (Current).last_column  = ZZRHSLOC (Rhs, N).last_column;  \
                (Current).filename     = ZZRHSLOC (Rhs, 1).filename;     \
            } \
            else \
           {                                                             \
                (Current).first_line   = (Current).last_line   =         \
                ZZRHSLOC (Rhs, 0).last_line;                             \
                (Current).first_column = (Current).last_column =         \
                ZZRHSLOC (Rhs, 0).last_column;                           \
                (Current).filename  = NULL;                              \
            }                                                            \
        while (0)

    #include <QString>
    #include "setofrules.h"
}

%{
    #pragma warning(disable: 4996)
    #include <QString>
    #include <QList>
    #include "setofrules.h"

    QList<SetOfRules> setsOfRules;

    extern int zzlex();
    extern void zzerror(const char *msg);
%}

%union
{
    int intVal;
    QString *strVal;
    Rule *rule;
    QList<Rule> *rules;
    SetOfRules *setOfRule;
    QList<int> *numbers;
}

%token<intVal> ZZNUMBER
%token<strVal> ZZID

%type<rule> Rule
%type<rules> Rules
%type<setOfRule> SetOfRules
%type<numbers> Numbers

%start SetsOfRules

%%
SetsOfRules
    : SetOfRules {setsOfRules << *$1; delete $1;}
    | SetsOfRules SetOfRules {setsOfRules << *$2; delete $2;}
    ;

SetOfRules
    : '@' ZZID Rules {$$ = new SetOfRules(*$2, *$3); delete $2; delete $3;}
    ;

Rules
    : Rule {$$ = new QList<Rule>(); *$$ << *$1; delete $1;}
    | Rules Rule {*$$ << *$2; delete $2;}
    ;

Rule
    : ZZID '=' ZZNUMBER ZZNUMBER '{' Numbers '}' {$$ = new Rule(*$1, $3, $4, *$6); delete $1; delete $6;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '}' {$$ = new Rule(*$1, $3, $4, QList<int>()); delete $1;}
    ;

Numbers
    : ZZNUMBER {$$ = new QList<int>(); *$$ << $1;}
    | Numbers ',' ZZNUMBER {*$$ << $3;}
    ;
%%
