#include <vld.h>
#include <QTextStream>
#include "worker.h"

int main(int argc, char *argv[])
{
    try
    {
        if (argc != 3)
        {
            throw QString("Invalid number of arguments");
        }

        Worker worker(argv[1], argv[2]);
        worker.run();
    }
    catch (const QString &e)
    {
        QTextStream out(stderr);
        out << e << endl;
    }

    return 0;
}
